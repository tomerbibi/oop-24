﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___24
{
    class MyLibrary
    {
        private Dictionary<string, Book> books = new Dictionary<string, Book>();
        public MyLibrary()
        {
        }
        public int Count
        { 

            get
            {
                return books.Count;
            }
        }
        public bool AddBook(Book b)
        {
            if (!books.ContainsKey(b.Titel))
            {
                books[b.Titel] = b;
                return true;
            }
            return false;
        }
        public bool RemoveBook(string title)
        {
            if (books.ContainsKey(title))
            {
                books.Remove(title);
                return true;
            }
            return false;
        }
        public bool HaveThisBook(string title)
        {
            return books.ContainsKey(title);
        }
        public Book GetBook (string title)
        {
            books.TryGetValue(title, out Book b);
            return b;
        }
        public Book GetBookByAuthor(string title)
        {
            Dictionary<string, Book> booksByAuthor = new Dictionary<string, Book>();
            foreach (KeyValuePair<string, Book> item in books)
            {
                booksByAuthor.Add(item.Value.Author, item.Value);
            }
            booksByAuthor.TryGetValue(title, out Book b);
            return b;
        }
        public void Clear()
        {
            books.Clear();
        }
        public List<string> GetAuthors()
        {
            List<string> authors = new List<string>();
            foreach (KeyValuePair<string, Book> item in books)
            {
                authors.Add(item.Value.Author);
            }
            return authors;
        }
        public List<Book> GetBooksSortedByAuthorName()
        {
            List<Book> sortedBooks = new List<Book>();
            foreach (KeyValuePair<string, Book> item in books)
            {
                sortedBooks.Add(item.Value);
            }
            sortedBooks.Sort(new BooksCompareByAuthor());
            return sortedBooks;
        }
        public List<string> GetBooksTitleSorted()
        {
            List<string> sortedTitels = new List<string>();
            foreach (KeyValuePair<string, Book> item in books)
            {
                sortedTitels.Add(item.Value.Titel);
            }
            sortedTitels.Sort();
            return sortedTitels;
        }
    }
}
