﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___24
{
    class Book
    {
        public Book(string titel, string content, string author, string category)
        {
            Titel = titel;
            Content = content;
            Author = author;
            Category = category;
        }

        public string Titel { get; private set; }
        public string Content { get; private set; }
        public string Author { get; private set; }
        public string Category { get; private set; }

        public override string ToString()
        {
            return $"title: {Titel}, content: {Content}, author: {Author}, Category: {Category}";
        }
    }
}
