﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___24
{
    class BooksCompareByAuthor : IComparer<Book>
    {
        public int Compare(Book x, Book y)
        {
           return x.Author.CompareTo(y.Author);
        }
    }
}
